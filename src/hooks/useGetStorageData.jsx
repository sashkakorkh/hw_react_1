import {useEffect} from "react";


export const useGetStorageData = (item, itemState, setItem) => {
    useEffect(() => {
        const itemArray = getItemFromStorage(item)
        const checkedItem = Array.isArray(itemArray) ? (itemArray.every((item) =>
            item.hasOwnProperty('name') &&
            item.hasOwnProperty('price') &&
            item.hasOwnProperty('currency') &&
            item.hasOwnProperty('imagePath') &&
            item.hasOwnProperty('sku') &&
            item.hasOwnProperty('color')
        )) : false

        if (checkedItem) {
            setItem(itemArray)
        } else {
            throw new Error('Помилка. Дані не завантажені');
        }
    }, [])

}

export const getItemFromStorage = (item) => {
    const itemFromStorage = localStorage.getItem(item);
    return  typeof itemFromStorage === 'string' ? JSON.parse(itemFromStorage) : [];
}