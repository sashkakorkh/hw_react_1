import PropTypes from "prop-types";
import React from "react";
import {ProductListContainer} from "../containers/productListContainer";


export const Favourites = ({
                               favourites,
                               setFavourites,
                               cart,
                               setCart,
                               page,

                           }) => {

    return (
        <>
            <ProductListContainer
                data={favourites}
                favourites={favourites}
                cart={cart}
                setFavourites={setFavourites}
                titleText={'Список бажань'}
                page={page}
                setCart={setCart}
            />
        </>
    );
};

Favourites.propTypes = {
    favourites: PropTypes.array.isRequired,
    setFavourites: PropTypes.func.isRequired,
    cart: PropTypes.array.isRequired,
    setCart: PropTypes.func.isRequired,
    page: PropTypes.string.isRequired,
};
