import PropTypes from 'prop-types';
import React from "react";
import {ProductListContainer} from "../containers/productListContainer";

export const Cart = ({
                         favourites,
                         setFavourites,
                         cart,
                         setCart,
                         page,
                     }) => {
    return (
        <>
            <ProductListContainer
                data={cart}
                favourites={favourites}
                cart={cart}
                setFavourites={setFavourites}
                titleText={'Корзина'}
                page={page}
                setCart={setCart}
            />
        </>
    );
};

Cart.propTypes = {
    favourites: PropTypes.array.isRequired,
    setFavourites: PropTypes.func.isRequired,
    cart: PropTypes.array.isRequired,
    setCart: PropTypes.func.isRequired,
    page: PropTypes.string.isRequired,
};
