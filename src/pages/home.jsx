import PropTypes from "prop-types";
import {ProductListContainer} from "../containers/productListContainer";


export const Home = ({
                         data,
                         favourites,
                         setFavourites,
                         cart,
                         setCart,
                         page,

                     }) => {


    return (
        <>
            <ProductListContainer
                data={data}
                favourites={favourites}
                cart={cart}
                setFavourites={setFavourites}
                titleText={'Чоловічі кросівки'}
                page={page}
                setCart={setCart}
            />
        </>
    );
};


Home.propTypes = {
    data: PropTypes.array.isRequired,
    favourites: PropTypes.array.isRequired,
    setFavourites: PropTypes.func.isRequired,
    cart: PropTypes.array.isRequired,
    setCart: PropTypes.func.isRequired,
    page: PropTypes.string.isRequired,
};


/*

export class Home extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <>
                <Header  favourites={this.props.favourites}
                         cart={this.props.cart}/>

                <MainContainer data={this.props.data}
                               favourites={this.props.favourites}
                               cart={this.props.cart}
                               addToFavourites={this.props.addToFavourites}
                               addToCart={this.props.addToCart}

                />
            </>
        )
    }
}

*/

