import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import  {createGlobalStyle} from "styled-components";
import { BrowserRouter } from "react-router-dom";

const Global = createGlobalStyle`

  *,
  *::before,
  *::after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    min-height: 100vh;;
    font-family:'Roboto-Regular', sans-serif;
    color: #000000;
  }

  a,button {
    transition: 0.2s all ease-in-out;
    outline: none;
  }
  
  main {
    margin-top: 5.1rem;
    width: 100%
  }
  
  li {
    list-style: none;
  }
  
  nav {
    & > ul {
      display: flex;
      justify-content: space-around;
    }
  }


  /* Preferred box-sizing value */
  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
  a {
    all: unset;
  }
  /* Reapply the pointer cursor for anchor tags */
  a, button {
    cursor: revert;
    
  }

  /* Remove list scss (bullets/numbers) */
  ol, ul, menu {
    list-style: none;
  }
  

  /* For images to not be able to exceed their container */
  img {
    max-width: 100%;
  }
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.25);
}
`


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <>
      <Global/>
      <BrowserRouter>
    <App />
      </BrowserRouter>
  </>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
