import styled, {keyframes} from "styled-components";
import PropTypes from "prop-types";
import {ProductCardContainer} from "../../containers/productCardContainer";
import {StyledOpenButton} from "../../containers/productCardContainer";

const animatezoom = keyframes`
  from {
    transform: scale(0)
  }
  to {
    transform: scale(1)
  }
`


const StyledMain = styled.main`
  margin-top: 6.1rem;

  & > div {
    display: flex;
    flex-wrap: wrap;
    align-items: flex-start;
    margin: 0 -1rem;
    border-top: 2px solid black;
  }

  & h1 {
    font: 29px 'Roboto-Medium', sans-serif;
    letter-spacing: .02em;
    line-height: 1;
    margin: 0.5rem;
    text-transform: uppercase
  }
`

const StyledModalContainer = styled.div`
  display: none;
  position: fixed;
  z-index: 1;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgba(0, 0, 0, 0.25);

  & > div {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 516px;
    min-height: 184px;
    margin: 20% auto;
    border-radius: 5px;
    box-shadow: 0 1px 6px 0 rgba(32, 33, 36, 0.28);
    position: relative;
    font-size: 22px;
    animation: ${animatezoom} 0.3s ease;
    font-family: 'Roboto-Regular', sans-serif;

    & header {
      width: 100%;
      display: flex;
      padding: 10px;
      align-items: center;
      justify-content: space-between;

      & h4 {
        color: white;
        font-size: .8em;
      }

      & span {
        transition: 0.2s all ease-in-out;

        &:hover {
          cursor: pointer;
          transform: scale(0.8);

        }
      }
    }

    & main {
      margin-top: 0.2em;

      & div {
        display: flex;
        align-items: center;
        flex-direction: column;
      }
    }
  }
`

const StyledActionButton = styled(StyledOpenButton)`
  padding: 10px 0;
  margin: 15px 3px;
`


export const ProductList = ({
                                favourites,
                                titleText,
                                children,
                                modalParams,
                                productSelected,
                                showModalState,
                                hideModal,
                                showModal,
                                actionsForModalBtns,
                                addToFavourites,
                                setSelectedProduct,
                                cart,
                                page

                            }) => {

    const showHideStyle = showModalState ? {display: 'block'} : {display: 'none'};

    const firstButtonAction = actionsForModalBtns[modalParams.actionBtnDo]

    const firstModalBtnRender = (
        <StyledActionButton
            backgroundColor={modalParams.backgroundColorBtnFirst}
            type={modalParams.typeBtnFirst}
            onClick={() => {
                hideModal(modalParams.modalId);
                firstButtonAction(productSelected);}}
        >
            {modalParams.textButtonFirst}
        </StyledActionButton>
    );

    const secondModalBtnRender = (
        <StyledActionButton
            backgroundColor={modalParams.backgroundColorBtnSecond}
            type={modalParams.typeBtnSecond}
            onClick={() => hideModal(modalParams.modalId)}
        >
            {modalParams.textButtonSecond}
        </StyledActionButton>
    );

    const actionButtonComponent = modalParams.action ? (
        <div>
            {firstModalBtnRender} {secondModalBtnRender}
        </div>
    ) : null;

    return (
        <StyledMain>
            <h1>{titleText}</h1>
            <div>
                {children}
            </div>
            <StyledModalContainer style={showHideStyle}
                                  onClick={hideModal}>
                <div style={{backgroundColor: modalParams.backgroundColor,color: modalParams.color }}
                     onClick={(e) => e.stopPropagation()}>
                    <header style={{backgroundColor: modalParams.backgroundColorHeader}}>
                        <h4>{modalParams.titleText}</h4>

                        {modalParams.closeIcon !== false ? (
                                <span onClick={() => hideModal(modalParams.modalId)}>
                                    <i className="fa-sharp fa-regular fa-rectangle-xmark" style={{color: '#fff'}}></i>
                                </span>
                        ) : null}
                    </header>
                    <main>
                        <div>
                            <ProductCardContainer product={productSelected}
                                                  showModal={showModal}
                                                  addToFavourites={addToFavourites}
                                                  favourites={favourites}
                                                  setSelectedProduct={setSelectedProduct}
                                                  cart={cart}
                                                  page={page}
                                                  isModal={true}
                            />
                        </div>
                    </main>
                    <footer>{actionButtonComponent}</footer>
                </div>
            </StyledModalContainer>
        </StyledMain>
    );
};


ProductList.propTypes = {
    favourites: PropTypes.array.isRequired,
    titleText: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    modalParams: PropTypes.shape({
        actionBtnDo: PropTypes.string,
        backgroundColorBtnFirst: PropTypes.string,
        typeBtnFirst: PropTypes.string,
        textButtonFirst: PropTypes.string,
        backgroundColorBtnSecond: PropTypes.string,
        typeBtnSecond: PropTypes.string,
        textButtonSecond: PropTypes.string,
        action: PropTypes.bool,
        modalId: PropTypes.string,
        backgroundColor: PropTypes.string,
        color: PropTypes.string,
        backgroundColorHeader: PropTypes.string,
        closeIcon: PropTypes.bool,
        titleText: PropTypes.string
    }),
    productSelected: PropTypes.object.isRequired,
    showModalState: PropTypes.bool.isRequired,
    hideModal: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired,
    actionsForModalBtns: PropTypes.object.isRequired,
    addToFavourites: PropTypes.func.isRequired,
    setSelectedProduct: PropTypes.func,
    cart: PropTypes.array.isRequired,
    page: PropTypes.string.isRequired
};