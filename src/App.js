import {Home} from "./pages";
import {Route, Routes} from "react-router-dom";
import {useEffect, useState} from "react";
import {Header} from "./components/header";
import {Cart} from "./pages";
import {Favourites} from "./pages";
import {getItemFromStorage, useGetStorageData} from "./hooks/useGetStorageData";


function App() {
    const [favourites, setFavourites] = useState([]);
    const [cart, setCart] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState([]);
    const [page,setPage] =useState('home')

    useEffect(() => {
        setIsLoading(true);
        fetch('./items.json')
            .then(res => res.json())
            .then(data => {
                setData(data);
                setIsLoading(false);
            })
            .catch(error => {
                console.error('Error:', error);
            });
        setPage(getItemFromStorage('page'))
        setIsLoading(false);
    }, []);

       useGetStorageData('cart', cart, setCart)
       useGetStorageData('favourites', favourites, setFavourites)


    useEffect(() => {
        localStorage.setItem('favourites', JSON.stringify(favourites));
    }, [favourites]);

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
    }, [cart]);

    useEffect(() => {
        localStorage.setItem('page', JSON.stringify(page));
    }, [page]);

    return (

        <div style={{width: 100 +'%',
                            padding: '0 2rem' }}>
            <Header favourites={favourites}
                    cart={cart}
                    setPage={setPage}/>
            {isLoading ? ('Checking data') : (
                <Routes>
                    <Route path="/" element={<Home
                                                   data={data}
                                                   favourites={favourites}
                                                   setFavourites={setFavourites}
                                                   page={page}
                                                   cart={cart}
                                                   setCart={setCart}
                    />}
                    />
                    <Route path="/cart" element={<Cart          setCart={setCart}
                                                                cart={cart}
                                                                favourites={favourites}
                                                                setFavourites={setFavourites}
                                                                page={page}/>
                    }/>
                    <Route path="/favourites" element={<Favourites
                                                                favourites={favourites}
                                                                cart={cart}
                                                                page={page}
                                                                setFavourites={setFavourites}
                                                                setCart={setCart}/>
                    }/>
                    <Route path="*" element={<div> not found</div>}/>
                </Routes>)}
        </div>);

}

export default App;