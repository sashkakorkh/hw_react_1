import styled from "styled-components";
import PropTypes from "prop-types";


const StyledCard = styled.div`
  margin: 2rem 0;
  outline: solid 1px transparent;
  padding: 0 1rem;
  width: 25%;
  max-height: 50%;

  &:hover {
    outline-color: #ccc;
  }

  &>div {
    border-bottom: solid 1px #ccc;
    position: relative;

    & img {
      display: block;
      margin: 0 auto;
      z-index: 0;
    }

    & span {
      position: absolute;
      top: 5px;
      right: -12px;
      z-index: 1;
      cursor: pointer;
    }
  }

  & div:not(:first-child) {
    margin: 0 auto;
    width: 80%;
    text-align: center;

    & div {
      margin: .8rem 0 0;
      min-height: 6.3rem;

      & p {
        font: 1rem 'Roboto-Regular', sans-serif;
      }
    }

    & p {
      font: 1rem 'Roboto-Medium', sans-serif;
      font-weight: bold;
    }
  }
`



export const StyledOpenButton = styled.button`
  color: white;
  min-width: 101px;
  padding: 10px;
  margin: 15px 0;
  cursor: pointer;
  border: transparent;
  transition: 0.2s all ease-in-out;
  background-color: ${props => props.backgroundColor};

  &:hover {
    transform: scale(0.9)
  }
`




export const ProductCardContainer = ({
                                         cart,
                                         product,
                                         showModal,
                                         addToFavourites,
                                         favourites,
                                         setSelectedProduct,
                                         page,
                                         isModal

                                     }) => {

    const isFavourite = favourites.some((item) => item.sku === product.sku)
    const isInCart = cart.some((item) => item.sku === product.sku)

    const handleAddToFavourites = (booleanFav) => {
        addToFavourites(booleanFav, product);
    };

    let crossIcon = (<>
        <span   style={{
                    position: 'absolute',
                    content: '',
                    top: 4+'px',
                    right: -20 + 'px',
                    height: 25 + 'px',
                    width: 25 + 'px',
                    cursor: 'pointer',}}
                onClick={() => {
                            showModal('delete-from-cart', true);
                            setSelectedProduct(product);}}
        >
                <i className="fa-sharp fa-regular fa-rectangle-xmark" style={{color: '#000000'}}></i>
        </span></>)


    let favIcon = (<>
        <span
            style={{display: isFavourite ? 'none' : 'block'}}
            onClick={() => {
                handleAddToFavourites(true);}}
        >
            <i className="fa-sharp fa-regular fa-star" style={{color: '#000000'}}></i>
        </span>
        <span
            style={{display: isFavourite ? 'block' : 'none'}}
            onClick={() =>
                handleAddToFavourites(false)}
        >
            <i className="fa-sharp fa-solid fa-star" style={{color: '#000000'}}></i>
        </span></>)


    const openModalButton = (
        <StyledOpenButton
                        backgroundColor={isInCart ? 'grey' : 'black'}
                        type={'button'}
                        disabled={isInCart}
                        onClick={() => {
                            showModal('add-to-cart', true);
                            setSelectedProduct(product);}}
        >
            {(isInCart ? 'Додано в кошик' : 'Додати в кошик')}
        </StyledOpenButton>)

    return (
        <StyledCard>
            <div style={{position: 'relative'}}>
                <img src={product.imagePath} alt={'sneakers'}/>
                {isModal ? null : (page === 'cart' ? crossIcon : favIcon)}
            </div>
            <div>
                <div>
                    <p>{product.name}</p>
                </div>

                <p>{product.price}</p>
                {(isModal || page === 'cart') ? null : openModalButton}
            </div>
        </StyledCard>
    )
};

ProductCardContainer.propTypes = {
    product: PropTypes.shape({
        price: PropTypes.number,
        name: PropTypes.string,
        currency: PropTypes.string,
        imagePath: PropTypes.string,
        sku: PropTypes.string,
        color: PropTypes.string,
    }),
    showModal: PropTypes.func.isRequired,
    favourites: PropTypes.array.isRequired,
    addToFavourites: PropTypes.func.isRequired,
    setSelectedProduct: PropTypes.func,
    cart: PropTypes.array.isRequired,
    page: PropTypes.string.isRequired,
    isModal:PropTypes.bool.isRequired,
};
