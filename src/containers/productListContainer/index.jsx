import {ProductList} from "../../components/ProductList";
import {ProductCardContainer} from "../productCardContainer";
import {useState} from "react";
import PropTypes from "prop-types";


export const ProductListContainer = ({
                                         titleText,
                                         data,
                                         favourites,
                                         setFavourites,
                                         cart,
                                         setCart,
                                         page,

                                     }) => {

    const [showModalState, setShowModalState] = useState(false);
    const [modalParams, setModalParams] = useState({});
    const [closeIcon, setCloseIcon] = useState(false);
    const [productSelected, setProductSelected] = useState({});

    const paramsForModalId = {

        'add-to-cart': {
            actionBtnDo: 'addToCart',
            titleText: 'Додати в корзину?',
            backgroundColorHeader: 'black',
            backgroundColor: 'white',
            closeIcon: true,
            action: true,
            backgroundColorBtnFirst: 'black',
            textButtonFirst: 'Додати',
            typeBtnFirst: 'button',
            backgroundColorBtnSecond: '#1c0202',
            textButtonSecond: 'Відміна',
            typeBtnSecond: 'button'

        }, 'delete-from-cart': {
            actionBtnDo: 'deleteFromCart',
            titleText: 'Ви хочете видалити цей товар з корзини?',
            backgroundColorHeader: '#cc3333',
            backgroundColor: '#ff3333',
            color: 'white',
            closeIcon: true,
            action: true,
            backgroundColorBtnFirst: '#aa3333',
            textButtonFirst: 'Так',
            typeBtnFirst: 'submit',
            backgroundColorBtnSecond: '#aa3333',
            textButtonSecond: 'Відміна',
            typeBtnSecond: 'button'
        }
    };


    const showModal = (modalId, closeIcon = true,) => {
        const params = paramsForModalId[modalId];
        if (params) {
            setModalParams(params);
        }
        setShowModalState(true);
        setCloseIcon(closeIcon);

    };


    const hideModal = () => {
        if (!closeIcon) return;
        setShowModalState(false);
    };

    const setSelectedProduct = (product) => {
        setProductSelected(product);
    };

    const addToFavourites = (addToFav, product) => {

        if (addToFav) {
            const updatedFavourites = [...favourites, product];
            setFavourites(updatedFavourites);

        } else if (!addToFav) {
            const updatedFavourites = favourites.filter(item => item.sku !== product.sku);
            setFavourites(updatedFavourites);
        }
    };

    const addToCart = product => {
        const updatedCart = [...cart, product]
        setCart(updatedCart)
    }


    const deleteFromCart = (product) => {
        const updatedCart = cart.filter(item => item.sku !== product.sku);
        setCart(updatedCart);
    }


    const actionsForModalBtns = {
        'addToCart': addToCart,
        'deleteFromCart': deleteFromCart,
    }


    const cards = data.map((product) => (
        <ProductCardContainer
            key={product.sku}
            product={product}
            showModal={showModal}
            addToFavourites={addToFavourites}
            favourites={favourites}
            setSelectedProduct={setSelectedProduct}
            cart={cart}
            page={page}
            isModal={false}
        />));

    return <ProductList titleText={titleText}
                        favourites={favourites}
                        modalParams={modalParams}
                        productSelected={productSelected}
                        showModalState={showModalState}
                        showModal={showModal}
                        hideModal={hideModal}
                        actionsForModalBtns={actionsForModalBtns}
                        addToFavourites={addToFavourites}
                        cart={cart}
                        setFavourites={setFavourites}
                        page={page}

    >
        {cards}
    </ProductList>;
};


ProductListContainer.propTypes = {
    data: PropTypes.array.isRequired,
    favourites: PropTypes.array.isRequired,
    setFavourites: PropTypes.func.isRequired,
    cart: PropTypes.array.isRequired,
    setCart: PropTypes.func.isRequired,
    page: PropTypes.string.isRequired,
    titleText: PropTypes.string.isRequired,
};


